# infix-el
Quick and simple infix math macro for elisp.

Example:
```lisp
*** Welcome to IELM ***  Type (describe-mode) or press C-h m for help.
ELISP> (infix 1 + 3 * 3)
10 (#o12, #xa, ?\C-j)
ELISP> (infix x = 3 * (1 + 1))
6 (#o6, #x6, ?\C-f)
ELISP> x
6 (#o6, #x6, ?\C-f)
ELISP> (infix y = 3 ^ (2 + 1))
27 (#o33, #x1b, ?\C-\[)
ELISP> y
27 (#o33, #x1b, ?\C-\[)

```

Order of operations is configurable by using the `infix--order` variable.  Operations can be added or removed from that list as well.

Operators which required translation are configured using the alist in the `infix--translation` variable.  For example, "^" is translated to `math-pow` and `=` is translated to `setf` by default.


# Notes:
There's an alternative infix math package with many more features here: https://github.com/rspeele/infix.el .

I believe the built in emacs `calc` package can also do this type of thing.
