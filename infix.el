;;; infix.el --- simple infix math macro  -*- lexical-binding: t -*-
;; Copyright (C) 2021  Free Software Foundation, Inc.

;; Author: Stephen Meister <>
;; URL: https://gitlab.com/steve-emacs-stuff/infix-el
;; Version: 0.0.1
;; Keywords: 

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Supported operations are found in the `infix-order' variable.  The
;; ordering of operators in the `infix--order' variable represents
;; their precedence, operators earlier in the list bind more tightly
;; than operators later in the list.
;;
;; By default, som operators are translated from the more familar
;; "math-ish" symbols to their lisp symbols.  This is done with the
;; alist in `infix--translation'.  By default this translates = to
;; setf and ^ to math-pow.
;;
;; This macro will not attempt to split symbols up.  This means that
;; things like "x + 2" can not be entered as "x+2".  The spaces
;; between symbols are important and must exist.
;;
;; Here are some exaples:
;; - translating "math-ish" symbols to lisp
;; (infix x = 3 ^ 2) => (setf x (math-pow 3 2))
;; - variables are fine.
;; (infix my-var + 3 * my-other-var) => (+ my-var (* 3 my-other-var))
;; - any type of symbol is passed through without changes.
;; (infix my+var + 3 * :my-other+var) => (+ my+var (* 3 :my-other+var))
;; - Correct but not optimal s-expressions are produced
;; (infix 1 + 2 + 3 + my-var) => (+ 1 (+ 2 (+ 3 my-var)))
;; - Nested parentheses are ok too:
;; (infix 4 * (2 + 3)) => (* 4 (infix 2 + 3)) => (* 4 (+ 2 3))

;;; Code:

(defvar infix--order '(^ * / + - =)
  "Order of operations for infix, parentheses are always first implicitly")

(defvar infix--translation '((^ math-pow) (= setf))
  "Translations for given infix operators to emacs functions")

(defmacro infix (&rest expr-symbols)
  "Do infix math?"
  (cl-flet ((translate-op (operator)
                (or (second (assoc operator infix--translation #'eq)) operator)))
    (let (
          ;; build up our operator order post translation
          (operator-order (mapcar (function translate-op) infix--order)))
      (cl-flet ((handle-parentheses (symbol-or-cons)
                  ;; wrap anything in parentheses with another infix
                  ;; macro to handle it later.
                  (if (consp symbol-or-cons)
                      (cons 'infix symbol-or-cons)
                    symbol-or-cons))
                (get-order (operator)
                  ;; more tightly binding operations will have a lower
                  ;; order number.
                  (or (cl-position operator operator-order)
                      (error "Unsupported operator: %s" operator)))
                (condense (back-stack)
                  ;; walk down the back-stack condensing individual
                  ;; operation node fragments.  Return the condensed
                  ;; output.
                  (while (cdr back-stack)
                    (let ((current-node (pop back-stack))
                          (next-node (first back-stack)))
                      (setf (first back-stack)
                            (list (first next-node) (second next-node) current-node))))
                  (first back-stack)))
        ;; back stack holds a list of partial nodes containing elements of
        ;; the form (OPERATOR ARGUMENT).  e.g. something like 1 + 2 + 3
        ;; will be ((+ 1) (+ 2)) with 3 remaining on the forward-stack
        (let ((back-stack (list (list (translate-op (second expr-symbols))
                                      (handle-parentheses (first expr-symbols)))))
              (forward-stack (cddr expr-symbols)))
          (while forward-stack
            (let ((last-node (first back-stack))
                  (next-arg (handle-parentheses (first forward-stack)))
                  (next-op (translate-op (second forward-stack))))
              (setq forward-stack (cddr forward-stack)) ;advance for next iteration
              (if (null next-op)
                  ;; all done.
                  (setf (first back-stack) (list (first last-node) (second last-node) next-arg))
                ;; not all done
                (if (< (get-order next-op) (get-order (first last-node)))
                    ;; this next operator is more important than the last one. stack this node onto the list.
                    (push (list next-op next-arg) back-stack)
                  ;; this next operator is less important than the last one (or equivalent)
                  (setf (first back-stack)
                        (list next-op
                              (list (first last-node) (second last-node) next-arg)))))))
          (condense back-stack))))))

;; quick test block - translate to ert?
;; (progn
;;   (assert (eq 3 (infix 1 + 2)))
;;   (assert (eq 3 (infix 5 - 2)))
;;   (assert (eq 6 (infix 1 + 2 + 3)))
;;   (assert (eq 6 (infix 10 - 2 * 2)))
;;   (assert (eq 18 (infix 10 * 2 - 2)))
;;   (assert (eq 20 (infix 10 * 3 - 5 * 2)))
;;   (assert (eq 19 (infix 10 + 2 * 5 - 1)))
;;   (assert (eq 13 (infix 1 + 3 * (1 + 3))))
;;   (assert (eq 19 (infix (1 + 3) * 4 + 3)))
;;   (assert (eq 17 (infix 1 + (1 + (2 + 3 * (1 + 1)) - 1) * 2)))
;;   (assert (eq 8 (let ((x 2))
;;                   (infix x = 2 + 3 * 2)
;;                   x))))

(provide 'infix)


